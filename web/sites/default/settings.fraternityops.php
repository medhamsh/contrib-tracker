<?php
/**
 * @file
 * FraternityOps settings.
 */

// Configure the DB.
if (getenv('PLATFORM') == 'FOPS') {
  $databases['default']['default'] = [
    'driver' => getenv('FOPS_DB_DRIVER'),
    'database' => getenv('FOPS_DB_DATABASE'),
    'username' => getenv('FOPS_DB_USERNAME'),
    'password' => getenv('FOPS_DB_PASSWORD'),
    'host' => getenv('FOPS_DB_HOST'),
    'port' => getenv('FOPS_DB_PORT'),
  ];
}

// Set trusted host.
if (getenv('PLATFORM') == 'FOPS' && getenv('FOPS_TRUSTED_HOST')) {
  $settings['trusted_host_patterns'] = ['^' . getenv('FOPS_TRUSTED_HOST') . '$'];
}

if (getenv('PLATFORM') == 'FOPS' && getenv('FOPS_HASH_SALT') && empty($settings['hash_salt'])) {
  $settings['hash_salt'] = getenv('FOPS_HASH_SALT');
}

// Configure the REDIS.
if (getenv('FOPS_REDIS_HOST') && !drupal_installation_attempted() && extension_loaded('redis')) {

  // Set Redis as the default backend for any cache bin not otherwise specified.
  $settings['cache']['default'] = 'cache.backend.redis';
  $settings['redis.connection']['host'] = getenv('FOPS_REDIS_HOST');
  $settings['redis.connection']['password'] = getenv('FOPS_REDIS_PASSWORD');
  $settings['redis.connection']['port'] = getenv('FOPS_REDIS_PORT');

  // Apply changes to the container configuration to better leverage Redis.
  // This includes using Redis for the lock and flood control systems, as well
  // as the cache tag checksum. Alternatively, copy the contents of that file
  // to your project-specific services.yml file, modify as appropriate, and
  // remove this line.
  $settings['container_yamls'][] = 'modules/contrib/redis/example.services.yml';

  // Allow the services to work before the Redis module itself is enabled.
  $settings['container_yamls'][] = 'modules/contrib/redis/redis.services.yml';

  // Manually add the classloader path, this is required for the container cache bin definition below
  // and allows to use it without the redis module being enabled.
  $class_loader->addPsr4('Drupal\\redis\\', 'modules/contrib/redis/src');

  // Use redis for container cache.
  // The container cache is used to load the container definition itself, and
  // thus any configuration stored in the container itself is not available
  // yet. These lines force the container cache to use Redis rather than the
  // default SQL cache.
  $settings['bootstrap_container_definition'] = [
    'parameters' => [],
    'services' => [
      'redis.factory' => [
        'class' => 'Drupal\redis\ClientFactory',
      ],
      'cache.backend.redis' => [
        'class' => 'Drupal\redis\Cache\CacheBackendFactory',
        'arguments' => ['@redis.factory', '@cache_tags_provider.container', '@serialization.phpserialize'],
      ],
      'cache.container' => [
        'class' => '\Drupal\redis\Cache\PhpRedis',
        'factory' => ['@cache.backend.redis', 'get'],
        'arguments' => ['container'],
      ],
      'cache_tags_provider.container' => [
        'class' => 'Drupal\redis\Cache\RedisCacheTagsChecksum',
        'arguments' => ['@redis.factory'],
      ],
      'serialization.phpserialize' => [
        'class' => 'Drupal\Component\Serialization\PhpSerialize',
      ],
    ],
  ];
}

